import org.junit.Test;
import static junit.framework.Assert.*;

public class GameTest {
    Game game = new Game();

    @Test
    public void testCreateCity() {
        City city = new City("Test City");
        assertNotNull(city);
    }

    @Test
    public void testAddCity() {
        City city = new City("Test City");
        assertEquals(city, game.addCity(city));
    }

    @Test
    public void testInsertNameOfCity() {
        String name = "Test Name";
        assertEquals(name, game.insertNameOfCity(name));
    }

    @Test
    public void testLettersAreEqual() {
        char a = 'a';
        char b = 'b';
        assertEquals(false, game.lettersAreEqual(a, b));
    }
}