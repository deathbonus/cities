import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;

public class Game {
    private ArrayList<City> cities;
    private Scanner scanner;
    private City currentCity;
    private City previousCity;
    private HashSet<String> listOfCitiesWithoutRepeats;

    public Game() {
        scanner = new Scanner(System.in);
        cities = new ArrayList<City>();
        listOfCitiesWithoutRepeats = new HashSet<String>();
    }

    public void start() {
        String insertedName = "";
        while (!insertedName.equalsIgnoreCase("quit")) {
            insertedName = insertNameOfCity(scanner.nextLine());
            currentCity = new City(insertedName);
            if (cities.isEmpty()) {
                addCity(currentCity);
            } else {
                if (lettersAreEqual(currentCity.getFirstLetterOfName(), previousCity.getLastLetterOfName())) {
                    addCity(currentCity);
                } else {
                    informAboutWrongName();
                }
            }
        }
        endGame();
    }

    public City addCity(City city) {
        cities.add(city);
        previousCity = city;
        listOfCitiesWithoutRepeats.add(city.getName());
        return cities.get(cities.size() - 1);
    }

    public boolean lettersAreEqual(char firstLetter, char lastLetter) {
        firstLetter = Character.toLowerCase(firstLetter);
        lastLetter = Character.toLowerCase(lastLetter);
        return firstLetter == lastLetter;
    }

    String insertNameOfCity(String name) {
        return name;
    }

    private void informAboutWrongName() {
    }

    public void endGame() {
        scanner.close();
        System.out.println(listOfCitiesWithoutRepeats.toString());
    }
}
