public class City {
    private String name;
    private char firstLetterOfName;
    private char lastLetterOfName;

    public City(String name) {
        this.name = name;
        firstLetterOfName = name.charAt(0);
        lastLetterOfName = name.charAt(name.length() - 1);
    }

    public String getName() {
        return name;
    }

    public char getFirstLetterOfName() {
        return firstLetterOfName;
    }

    public char getLastLetterOfName() {
        return lastLetterOfName;
    }
}
